import os
from pathlib import Path

def iter_trace_paths(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".trace") or file.endswith(".rdc"):
                yield Path(root) / file

def trace_has_images(trace_path, imagedir):
    tracename = trace_path.name
    refdir = str(trace_path.parent / imagedir)
    if not os.path.isdir(refdir):
        return False
    for entry in os.scandir(refdir):
        if entry.is_file() and entry.name.startswith(tracename) and entry.name.endswith(".png"):
                return True
    return False
