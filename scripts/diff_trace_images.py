#!/usr/bin/env python3

import argparse
import glob
import os
import shutil
import subprocess
import sys
from pathlib import Path
from traceutil import iter_trace_paths, trace_has_images

def log(severity, msg):
    print("[diff_trace_images] %s: %s" % (severity, msg))

def find_traces_with_images(directory, device_name):
    traces = []
    for trace_path in iter_trace_paths(directory):
        if trace_has_images(trace_path, str(Path("references") / device_name)):
            if trace_has_images(trace_path, str(Path("test") / device_name)):
                traces.append(str(trace_path))
            else:
                log("Warning", "%s has reference but no test images, skipping" % str(trace_path))
    return traces

def copy_images_to_outputdir(trace, outputdir, device_name):
    trace_path = Path(trace)
    tracedir = str(trace_path.parent.name)
    tracename = str(trace_path.name)

    refprefix = str(trace_path.parent / "references" / device_name / trace_path.name) + "-"
    testprefix = str(trace_path.parent / "test" / device_name / trace_path.name) + "-"

    refdest = str(Path(outputdir) / tracedir / tracename / "references" / device_name)
    testdest = str(Path(outputdir) / tracedir / tracename / "test" / device_name)

    os.makedirs(refdest, exist_ok=True)
    os.makedirs(testdest, exist_ok=True)

    for f in glob.glob(refprefix + '*.png'):
        shutil.copy(f, refdest)

    for f in glob.glob(testprefix + '*.png'):
        shutil.copy(f, testdest)

    return str(Path(outputdir) / tracedir / tracename)

def diff_images(imagedir, device_name):
    cmd = ["apitrace", "diff-images", "--output=summary.html",
           str(Path("references") / device_name),
           str(Path("test") / device_name)]
    ret = subprocess.call(cmd, cwd=imagedir)
    return ret == 0

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('tracepath', help="single trace, or dir to walk recursively")
    parser.add_argument('--output-dir', required=True,
                        help="directory to write results to")
    parser.add_argument('--device-name', required=True,
                        help="the name of the graphics device used to produce images")

    args = parser.parse_args()

    traces = []
    if os.path.isdir(args.tracepath):
        traces.extend(find_traces_with_images(args.tracepath, args.device_name))
    elif os.path.isfile(args.tracepath):
        traces.append(args.tracepath)

    failed_diff = False

    for trace in traces:
        imageout = copy_images_to_outputdir(trace, args.output_dir, args.device_name)
        images_match = diff_images(imageout, args.device_name)
        if not images_match:
            log("Info", "Images differ for %s" % trace)
            failed_diff = True
        else:
            log("Info", "Images match for %s" % trace)

    sys.exit(1 if failed_diff else 0)

if __name__ == "__main__":
    main()
