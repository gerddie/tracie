#!/usr/bin/env python3

import sys
import subprocess
import re

def is_vendor_string(s):
    return "Vendor:" in s

def is_device_string(s):
    return "Device:" in s

def id_from_string(s):
    match = re.search("\((0x[0-9A-Fa-f]+)\)$", s)
    if match:
        return match[1]
    return None

def vendor_from_string(s):
    if "Intel" in s:
        return "intel"
    elif "VMware" in s:
        return "vmware"
    else:
        return id_from_string(s)

def device_from_string(s):
    if "softpipe" in s:
        return "softpipe"
    elif "llvmpipe" in s:
        return "llvmpipe"
    else:
        return id_from_string(s)

vendor = None
device = None

res = subprocess.run(["glxinfo"], capture_output=True)

for l in res.stdout.decode("utf-8").splitlines():
    if is_vendor_string(l):
        vendor = vendor_from_string(l)
    elif is_device_string(l):
        device = device_from_string(l)

    if vendor is not None and device is not None:
        break

if vendor is None or device is None:
    sys.exit(1)

print("%s-%s" % (vendor, device))
