#!/bin/sh

which glxinfo
if test $? -eq 1; then
	echo glxinfo not installed. Exiting.
	exit 1
fi

/usr/local/bin/Xvfb $DISPLAY -screen 0 1024x768x24 >/tmp/xvfb.log 2>&1 &
# Random sleep, otherwise glxinfo will start before Xvfb is up
sleep 3s
local_mesa=0
glxinfo | grep -q $MESA_SHA && local_mesa=1
if test $local_mesa -eq 0; then
	echo NOT using the locally built Mesa. Exiting.
	exit 1
fi
