import sys
from pathlib import Path

import renderdoc as rd

def findDrawWithEventId(controller, eventId):
   for d in controller.GetDrawcalls():
      if d.eventId == eventId:
         return d

   return None

def dumpImage(controller, eventId, outputDir, tracefile):
    draw = findDrawWithEventId(controller, eventId)
    if draw is None:
        raise RuntimeError("Couldn't find draw call with eventId " + str(eventId))

    controller.SetFrameEvent(draw.eventId, True)

    texsave = rd.TextureSave()

    # Select the first color output
    texsave.resourceId = draw.outputs[0]

    if texsave.resourceId == rd.ResourceId.Null():
        return

    filepath = Path(outputDir)
    filepath.mkdir(parents = True, exist_ok = True)
    filepath = filepath / (tracefile + "-" + str(int(draw.eventId)) + ".png")

    print("Saving image at eventId %d: %s to %s" % (draw.eventId, draw.name, filepath))

    # Most formats can only display a single image per file, so we select the
    # first mip and first slice
    texsave.mip = 0
    texsave.slice.sliceIndex = 0

    # For formats with an alpha channel, preserve it
    texsave.alpha = rd.AlphaMapping.Preserve
    texsave.destType = rd.FileType.PNG
    controller.SaveTexture(texsave, str(filepath))

def loadCapture(filename):
    cap = rd.OpenCaptureFile()

    status = cap.OpenFile(filename, '', None)

    if status != rd.ReplayStatus.Succeeded:
        raise RuntimeError("Couldn't open file: " + str(status))
    if not cap.LocalReplaySupport():
        raise RuntimeError("Capture cannot be replayed")

    status,controller = cap.OpenCapture(None)

    if status != rd.ReplayStatus.Succeeded:
        raise RuntimeError("Couldn't initialise replay: " + str(status))

    return (cap, controller)

def renderdoc_dump_images(filename, eventIds, outputDir):
   cap,controller = loadCapture(filename);

   tracefile = Path(filename).name

   for eventId in eventIds:
      dumpImage(controller, eventId, outputDir, tracefile)

   controller.Shutdown()
   cap.Shutdown()

if __name__ == "__main__":
   if len(sys.argv) < 3:
      raise RuntimeError("Usage: renderdoc_dump_images.py <trace> <draw-id>...")

   eventIds = [int(e) for e in sys.argv[2:]]

   renderdoc_dump_images(sys.argv[1], eventIds, '.')
