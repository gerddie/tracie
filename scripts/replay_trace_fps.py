#!/usr/bin/python3

import os
import re
import sys
import subprocess
from pathlib import Path
from traceutil import iter_trace_paths

def log(severity, msg):
    print("[replay_traces] %s: %s" % (severity, msg))

def parse_output(output):
    match = re.search('(\d+.\d+) fps', output)
    if not match:
        raise RuntimeError("Result line can't be located")
    return match[1]

def replay_from_trace(trace):
    NB_REPLAY = 3
    if trace.endswith('.trace'):
        log("Info", "Replaying trace %s" % trace)
        max_fps = 0.0
        cmd = ["apitrace", "replay", "-b", trace]
        for x in range(0, NB_REPLAY):
            ret = subprocess.check_output(cmd).decode('UTF-8')
            if ret:
                fps = float(parse_output(ret))
                max_fps = max(fps, max_fps)
            else:
                raise RuntimeError("Apitrace replay failed!")
        print(trace + ": Best of " + str(NB_REPLAY) + " replay runs: " + str(max_fps) + " FPS")
    else:
        log("Info", "Extension not supported yet %s" % trace)

def find_traces(directory):
    traces = []
    for trace_path in iter_trace_paths(directory):
        traces.append(str(trace_path))
    return traces

def main():
    if len(sys.argv) != 2:
        raise RuntimeError("usage: replay_trace_fps <tracefile or dir>")

    arg = sys.argv[1]

    traces = []
    if os.path.isdir(arg):
        traces.extend(find_traces(arg))
    elif os.path.isfile(arg):
        traces.append(arg)

    for trace in traces:
        replay_from_trace(trace)

if __name__ == "__main__":
    main()
