Required:
- Install and use Mesa master
- Script to replay and compare all the traces in traces-db
- Add a performance counter

Optional:
- Build and use Apitrace master
